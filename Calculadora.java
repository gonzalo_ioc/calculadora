package ioc.xtec.cat.calculadora;

import java.util.Scanner;
			
public class Calculadora {

    public static void main(String[] args) {
    

    	Calculadora calc = new Calculadora(); 
         Scanner scanner = new Scanner(System.in);

        System.out.println("Introdueix el primer número: ");
        double num1 = scanner.nextDouble();

        System.out.println("Introdueix el segon número: ");
        double num2 = scanner.nextDouble();

        System.out.println("Suma: " + calc.sumar(num1, num2));
        System.out.println("Resta: " + calc.restar(num1, num2));
        System.out.println("Multiplicación: "+ calc.Multiplicacion(num1, num2));
        
        try {
            System.out.println("Division: " + calc.Division(num1, num2));
        } catch (ArithmeticException e) {
            System.out.println("Error: " + e.getMessage());
        } finally {
            scanner.close(); 
        }
        /**/
    }
    
    double sumar(double num1, double num2) {
    	return num1 + num2;
    }
    
    double restar(double num1, double num2) {
    	return num1 - num2;
    }
    
    double Multiplicacion(double num1, double num2) {
    	return num1 * num2;
    }
    double Division(double num1, double num2) throws ArithmeticException{
    	
 if(num2 == 0) {
	 throw new ArithmeticException("No se puede dividir por 0");
 }else {
    		return num1 / num2;
 
 }
    }
    
}


